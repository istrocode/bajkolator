package eu.istrocode.bajkolator.api

import com.google.android.gms.maps.model.LatLng
import eu.istrocode.bajkolator.api.annotation.ServiceStationData
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.dto.StationData
import eu.istrocode.bajkolator.util.DateUtils
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.mozilla.javascript.NativeArray
import org.mozilla.javascript.NativeObject
import retrofit2.Converter
import retrofit2.Retrofit
import timber.log.Timber
import java.io.IOException
import java.lang.reflect.Type
import kotlin.math.roundToInt

class StationMapConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, StationData>? {
        for (annotation in annotations!!) {
            if (annotation.annotationClass.javaObjectType == ServiceStationData::class.java) {
                return StationMapResponseBodyConverter()
            }
        }
        return null
    }

    internal class StationMapResponseBodyConverter : Converter<ResponseBody, StationData> {

        @Throws(IOException::class)
        override fun convert(responseBody: ResponseBody): StationData {
            return getStationMapData(responseBody.string())
        }
    }

    companion object {

        fun create(): Converter.Factory {
            return StationMapConverterFactory()
        }

        private fun getStationMapData(html: String): StationData {
            val doc = Jsoup.parse(html)
            Timber.d("Page loaded")
            val stationNames = getStationNames(doc)
            val scripts = doc.getElementsByTag("script")
            for (script in scripts) {
                val result = script.data()
                if (result.contains("gMap.markerData")) {
                    Timber.d("Script with stations found")
                    val resultString = "var gMap = new Object();" +
                            "gMap.markerData = [];" +
                            "gMap.markerData" + result.substringAfter("gMap.markerData").substringBefore("}];") +
                            "}];"
                    return StationData(getStations(resultString, stationNames))
                }
            }
            throw IllegalStateException("No stations found")
        }

        private fun getStationNames(doc: Document): ArrayList<Pair<Int, String>> {
            val stationNameList = ArrayList<Pair<Int, String>>()
            doc.select("#cmeStationInfo > tbody").first().children().forEach {
                val stationNumber = it.getElementsByAttributeValue("data-title", "Číslo stanice").text().toInt()
                val stationName = it.getElementsByAttributeValue("data-title", "Názov stanice").text()
                stationNameList.add(Pair(stationNumber, stationName))
            }
            return stationNameList
        }

        private fun getStations(stationsString: String?, stationNameList: ArrayList<Pair<Int, String>>): List<Station> {

            val precipitationStationData = java.util.ArrayList<Station>()
            Timber.d("Stations extracted")

            //rhino implemetation
            val cx = org.mozilla.javascript.Context.enter()
            cx.optimizationLevel = -1
            val scope = cx.initStandardObjects()

            cx.evaluateString(scope, stationsString, "<cmd>", 1, null)

            val arr = (scope.get("gMap", scope) as NativeObject)["markerData"] as NativeArray
            val stationArray = arrayOfNulls<Any>(arr.length.toInt())
            for (o in arr.ids) {
                val index = o as Int
                stationArray[index] = arr.get(index, null)
                val station = stationArray[index] as NativeObject
                val stationNumber = (station.get("station_nr", null) as String).toInt()
                val title = stationNameList.firstOrNull { it.first == stationNumber }?.second ?: ""
                val img = station.get("img", null) as String
                val lat = (station.get("lat", null) as String).toDouble()
                val lon = (station.get("lng", null) as String).toDouble()
                val docks = (station.get("docks", null) as String).toInt()
                val bikes = (station.get("bikes", null) as Double).roundToInt()
//                val ebikes = (station.get("ebikes", null) as String).toInt()
                val refreshString = station.get("refresh", null) as String?
                val refresh = if (refreshString.isNullOrEmpty()) {
                    null
                } else {
                    DateUtils.parseDate(refreshString)
                }

                val p = Station(title,
                        station_nr = stationNumber,
                        bikes = bikes,
                        docks = docks,
//                        ebikes = ebikes,
                        img = img,
                        refresh = refresh,
                        latLng = LatLng(lat, lon)
                )
                precipitationStationData.add(p)
            }
            return precipitationStationData
        }
    }
}
