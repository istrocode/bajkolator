package eu.istrocode.bajkolator.api

import eu.istrocode.bajkolator.api.annotation.ServiceStationData
import eu.istrocode.bajkolator.dto.StationData
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface WebService {

    @get:ServiceStationData
    @get:GET("mapa-stanic")
    val stationMap: Deferred<StationData>
}