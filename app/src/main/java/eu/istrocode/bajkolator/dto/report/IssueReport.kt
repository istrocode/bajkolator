package eu.istrocode.bajkolator.dto.report

import eu.istrocode.bajkolator.dto.Station
import java.util.Date

data class IssueReport(
        val station: Station,
        val bikeNumber: String,
        val reason: Reason,
        val message: String?,
        val timestamp: Date)
