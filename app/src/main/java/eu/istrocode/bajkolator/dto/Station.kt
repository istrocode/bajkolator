package eu.istrocode.bajkolator.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import java.util.Date

class Station(
        val title: String,
        val station_nr: Int,
        val latLng: LatLng,
        val docks: Int,
        val img: String? = null,
        val bikes: Int? = null,
        val ebikes: Int? = null,
        val refresh: Date? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readParcelable(LatLng::class.java.classLoader)!!,
            parcel.readInt(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readSerializable() as? Date
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeInt(station_nr)
        parcel.writeParcelable(latLng, flags)
        parcel.writeInt(docks)
        parcel.writeString(img)
        parcel.writeValue(bikes)
        parcel.writeValue(ebikes)
        parcel.writeSerializable(refresh)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Station> {
        override fun createFromParcel(parcel: Parcel): Station {
            return Station(parcel)
        }

        override fun newArray(size: Int): Array<Station?> {
            return arrayOfNulls(size)
        }
    }

}



