package eu.istrocode.bajkolator.dto

import android.os.Build
import eu.istrocode.bajkolator.R

enum class AppThemeMode(val nameResId: Int, private val requiredApi: Int = 1) {
    DAY(R.string.app_theme_mode_light),
    NIGHT(R.string.app_theme_mode_dark),
    AUTO(R.string.app_theme_mode_auto),
    SYSTEM(R.string.app_theme_mode_system, 29);

    companion object {
        fun getModes(): Array<AppThemeMode> {
            val modes: MutableList<AppThemeMode> = ArrayList()
            AppThemeMode.values().forEach {
                if (Build.VERSION.SDK_INT >= it.requiredApi) {
                    modes.add(it)
                }
            }
            return modes.toTypedArray()
        }
    }
}