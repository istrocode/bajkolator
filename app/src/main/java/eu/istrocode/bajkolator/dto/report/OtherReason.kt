package eu.istrocode.bajkolator.dto.report

import eu.istrocode.bajkolator.R

class OtherReason: Reason {
    override fun getTextResId(): Int {
        return R.string.issue_reason_other
    }
}