package eu.istrocode.bajkolator.dto

import com.google.gson.annotations.SerializedName

data class LatLng(
        @SerializedName("latitude") @JvmField val latitude: Double? = null,
        @SerializedName("longitude") @JvmField val longitude: Double? = null
)