package eu.istrocode.bajkolator.dto.report

class StandardReason(val reasonResId: Int) : Reason {
    override fun getTextResId(): Int {
        return reasonResId
    }
}
