package eu.istrocode.bajkolator.util

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds

class LocationUtils {
    companion object {
        val bounds: LatLngBounds
            get() = LatLngBounds(LatLng(48.093966, 17.063527),
                    LatLng(48.173171, 17.221964))

        fun getLatLng(location: Location): LatLng {
            return LatLng(location.latitude, location.longitude)
        }
    }
}
