package eu.istrocode.bajkolator.util

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


class DateUtils {
    companion object {
        fun parseDate(dateString: String): Date {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            return sdf.parse(dateString)
        }

        fun formatTimeSpannable(date: Date): SpannableStringBuilder {
            val hoursMinutes = SimpleDateFormat("HH:mm", Locale.US).format(date)
            val seconds = SimpleDateFormat(":ss", Locale.US).format(date)
            val builder = SpannableStringBuilder()

            val hmSpan = SpannableString(hoursMinutes)
            hmSpan.setSpan(StyleSpan(Typeface.BOLD), 0, hmSpan.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            builder.append(hmSpan)
            builder.append(SpannableString(seconds))
            return builder
        }

        fun formatDateTime(date: Date): String {
            val sfd = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US).format(date)
            return sfd.format(date)
        }
    }
}