package eu.istrocode.bajkolator.util

import android.content.Context
import android.content.res.Configuration
import eu.istrocode.bajkolator.dto.AppThemeMode

object DayNightUtil {
    fun isNightModeActive(context: Context): Boolean {
        val currentNightMode = context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return when (currentNightMode) {
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_YES -> true
            Configuration.UI_MODE_NIGHT_UNDEFINED -> false
            else -> false
        }
    }

    fun getDefaultMode(): AppThemeMode {
        return if(AppThemeMode.getModes().contains(AppThemeMode.SYSTEM)) {
            AppThemeMode.SYSTEM
        } else {
            AppThemeMode.DAY
        }
    }
}
