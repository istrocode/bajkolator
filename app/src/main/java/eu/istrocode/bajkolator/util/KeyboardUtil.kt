package eu.istrocode.bajkolator.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


object KeyboardUtil {

    fun setImeVisibility(view: View, visible: Boolean) {
        if (visible) {
            GlobalScope.launch(context = Dispatchers.Main) {
                val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(view, 0)
            }
        } else {
            GlobalScope.launch(context = Dispatchers.Main) {
                val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}