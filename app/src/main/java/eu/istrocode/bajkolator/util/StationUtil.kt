package eu.istrocode.bajkolator.util

import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.Station

class StationUtil {
    companion object {
        fun getColorRes(station: Station): Int {
            return when {
                station.bikes == null -> R.color.station_unavailable
                station.bikes == 0 -> R.color.station_empty
                station.bikes >= station.docks -> R.color.station_full
                station.bikes > 0 -> R.color.station_available
                else -> R.color.station_unavailable
            }
        }

        fun getMarkerRes(station: Station): Int {
            return when {
                station.bikes == null -> R.drawable.ic_station_unavailable
                station.bikes == 0 -> R.drawable.ic_station_empty
                station.bikes >= station.docks -> R.drawable.ic_station_full
                station.bikes > 0 -> R.drawable.ic_station_available
                else -> R.drawable.ic_station_unavailable
            }
        }
    }
}