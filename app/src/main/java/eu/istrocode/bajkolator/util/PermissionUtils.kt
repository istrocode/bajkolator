package eu.istrocode.bajkolator.util

import android.Manifest
import android.app.Activity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import eu.istrocode.bajkolator.R

class PermissionUtils {

    companion object {

        fun requestLocationPermission(fragment: Fragment, coordinatorLayout: CoordinatorLayout, requestCode: Int) {
            if (fragment.shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make(coordinatorLayout, R.string.location_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.dialog_ok) {
                            if (fragment.isAdded) {
                                fragment.requestPermissions(
                                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                        requestCode)
                            }
                        }
                        .show()
            } else {
                fragment.requestPermissions(
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        requestCode)
            }
        }

        fun requestLocationPermission(activity: Activity, coordinatorLayout: CoordinatorLayout, requestCode: Int) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make(coordinatorLayout, R.string.location_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.dialog_ok) {
                            ActivityCompat.requestPermissions(
                                    activity,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    requestCode)
                        }
                        .show()
            } else {
                ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        requestCode)
            }
        }

        fun showPermissionNotGranted(coordinatorLayout: CoordinatorLayout) {
            Snackbar.make(coordinatorLayout, R.string.location_permission_denied, Snackbar.LENGTH_LONG)
                    .show()
        }
    }
}
