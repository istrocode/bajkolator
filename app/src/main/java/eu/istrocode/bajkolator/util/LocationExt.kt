package eu.istrocode.bajkolator.util

import android.location.Location
import com.google.android.gms.maps.model.LatLng

fun LatLng.distanceTo(targetPos: LatLng): Float {
    val loc1 = Location("loc1")
    loc1.latitude = latitude
    loc1.longitude = longitude

    val loc2 = Location("loc2")
    loc2.latitude = targetPos.latitude
    loc2.longitude = targetPos.longitude

    return loc1.distanceTo(loc2)
}