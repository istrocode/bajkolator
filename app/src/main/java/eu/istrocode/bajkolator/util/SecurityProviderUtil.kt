package eu.istrocode.bajkolator.util

import android.content.Context
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import timber.log.Timber

object SecurityProviderUtil {

    /**
     * Uses Google Play Services to enable TLS1.2 on older devices
     *
     * @param client OKHtp client builder
     * @return
     */
    fun installServiceProviderIfNeeded(context: Context) {
        try {
            ProviderInstaller.installIfNeeded(context)
        } catch (e: GooglePlayServicesRepairableException) {
            Timber.e(e)
        } catch (e: GooglePlayServicesNotAvailableException) {
            Timber.e(e)
        }
    }
}
