package eu.istrocode.bajkolator.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.model.MapViewModel
import timber.log.Timber

class EnterPinDialog : DialogFragment() {

    lateinit var mapViewModel: MapViewModel
    lateinit var input: EditText


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mapViewModel = ViewModelProviders.of(activity!!).get(MapViewModel::class.java)
        input.requestFocus()
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val layout = LayoutInflater.from(context).inflate(R.layout.layout_dialog_enter_pin, null)
        input = layout.findViewById(R.id.edit_pin)

        return AlertDialog.Builder(context!!, R.style.AppTheme_Dialog)
                .setTitle(R.string.dialog_enter_pin_title)
                .setView(layout)

                .setNegativeButton(R.string.dialog_cancel) { dialogInterface, _ ->
                    dialogInterface.cancel()
                }
                .setPositiveButton(R.string.dialog_set_pin) { dialogInterface, _ ->
                    if (input.text.isBlank()) {
                        Timber.i("Entered blank pin")
                        dialogInterface.dismiss()
                    } else {
                        Timber.i("Entered pin: ${input.text}")
                        mapViewModel.setPinValue(input.text.toString())
                        dialogInterface.dismiss()
                    }
                }
                .create()
    }

    companion object {
        fun newInstance(): DialogFragment {
            return EnterPinDialog()
        }
    }
}