package eu.istrocode.bajkolator.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.model.MapViewModel

class RemovePinDialog : DialogFragment() {

    private lateinit var mapViewModel: MapViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mapViewModel = ViewModelProviders.of(activity!!).get(MapViewModel::class.java)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        return AlertDialog.Builder(context!!, R.style.AppTheme_Dialog)
                .setTitle(R.string.dialog_remove_pin_title)
                .setNegativeButton(R.string.dialog_cancel) { dialogInterface, _ ->
                    dialogInterface.cancel()
                }
                .setPositiveButton(R.string.dialog_remove_confirm) { dialogInterface, _ ->
                    mapViewModel.removePin()
                    dialogInterface.cancel()
                }
                .create()
    }

    companion object {
        fun newInstance(): DialogFragment {
            return RemovePinDialog()
        }
    }
}