package eu.istrocode.bajkolator.fragment

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.AppThemeMode
import eu.istrocode.bajkolator.model.MapViewModel


class DayNightDialog : DialogFragment() {

    var model: MapViewModel? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val defaultMode = AppThemeMode.valueOf(arguments!!.getString(currentMode)!!)
        val index = AppThemeMode.getModes().indexOf(defaultMode)

        val items: Array<CharSequence> = AppThemeMode.getModes().map { s ->
            getString(s.nameResId) as CharSequence
        }.toTypedArray()

        val builder = AlertDialog.Builder(context!!, R.style.AppTheme_Dialog)
                .setTitle(R.string.dialog_title_day_night_mode)
                .setSingleChoiceItems(items, index) { _, _ -> }
                .setCancelable(true)
                .setPositiveButton(R.string.dialog_ok) { dialog, _ ->
                    run {
                        val listView = (dialog as AlertDialog).listView
                        val mode = AppThemeMode.getModes()[listView.checkedItemPosition]
                        model!!.setDayNightMode(mode)
                        dismiss()
                    }
                }
                .setNegativeButton(R.string.dialog_cancel) { _, _ -> dismiss() }

        return builder.create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model = ViewModelProviders.of(activity!!).get(MapViewModel::class.java)
    }

    companion object {

        private const val currentMode: String = "currentMode"

        fun newInstance(mode: AppThemeMode): DayNightDialog {
            val args = Bundle()
            args.putString(currentMode, mode.name)
            val fragment = DayNightDialog()
            fragment.arguments = args
            return fragment
        }
    }
}