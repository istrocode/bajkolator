package eu.istrocode.bajkolator.model

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import eu.istrocode.bajkolator.LocatorApplication
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.api.WebService
import eu.istrocode.bajkolator.dto.AppThemeMode
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.dto.StationData
import eu.istrocode.bajkolator.dto.StationsBasicData
import eu.istrocode.bajkolator.dto.report.IssueReport
import eu.istrocode.bajkolator.preferences.Prefs
import eu.istrocode.bajkolator.util.DayNightUtil
import eu.istrocode.bajkolator.util.SingleLiveEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MapViewModel : ViewModel() {

    @Inject
    lateinit var service: WebService

    @Inject
    lateinit var application: Application

    @Inject
    lateinit var prefs: Prefs

    val stationsLiveData: MutableLiveData<StationData> = MutableLiveData()
    var stationsBasicData: StationData? = null
    val updateLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val failureLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val pinDialog: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val nightMode: SingleLiveEvent<AppThemeMode> = SingleLiveEvent()
    val appThemeDialog: SingleLiveEvent<AppThemeMode> = SingleLiveEvent()
    val recreateActivity: SingleLiveEvent<Void> = SingleLiveEvent()
    val snackbarMessage: SingleLiveEvent<Int> = SingleLiveEvent()
    val stationDetailDialog: SingleLiveEvent<Station> = SingleLiveEvent()
    val reportIssueDialog: SingleLiveEvent<Station?> = SingleLiveEvent()
    val sendReport: SingleLiveEvent<IssueReport> = SingleLiveEvent()
    val navigateTo: SingleLiveEvent<LatLng> = SingleLiveEvent()
    var userPin: String? = null
    var position: CameraPosition? = null
    var isZoomedToLocation = false
    @Volatile
    private var lastUpdate: Long = 0

    private val serialExecutor: ExecutorService = Executors.newSingleThreadExecutor()

    init {
        LocatorApplication.INSTANCE.component.inject(this)
        userPin = prefs.userPin
        serialExecutor.execute { loadStationBasicData() }
        if (prefs.dayNightMode != null) {
            nightMode.value = AppThemeMode.valueOf(prefs.dayNightMode!!)
            recreateActivity.call()
        } else {
            nightMode.value = DayNightUtil.getDefaultMode()
        }
    }

    private fun loadStationBasicData() {
        val jsonString = String(application.resources.openRawResource(R.raw.stations).readBytes())
        val stationBasicData = Gson().fromJson(jsonString, StationsBasicData::class.java)
        val stationInfo = ArrayList<Station>()
        stationBasicData.stations.iterator().forEach { station ->
            if (station.id != null &&
                    station.title != null &&
                    station.docks != null &&
                    station.latLng != null &&
                    station.latLng.latitude != null &&
                    station.latLng.longitude != null
            ) {
                stationInfo.add(Station(
                        station_nr = station.id,
                        title = station.title,
                        docks = station.docks.toInt(),
                        latLng = LatLng(station.latLng.latitude, station.latLng.longitude))
                )
            }
        }
        stationsBasicData = StationData(stationInfo)
    }

    fun loadData() {
        updateLiveData.value = true
        GlobalScope.launch {
            try {
                val data = service.stationMap.await()
                Timber.i("onResponse")
                updateLiveData.postValue(false)
                serialExecutor.execute { stationsLiveData.postValue(data) }
                failureLiveData.postValue(false)
            } catch (exception: Exception) {
                Timber.e("onFailure: $exception")
                updateLiveData.postValue(false)
                failureLiveData.postValue(true)
                serialExecutor.execute { stationsLiveData.postValue(stationsBasicData) }
            }
            lastUpdate = System.currentTimeMillis()
        }
    }

    fun removePin() {
        prefs.userPin = null
        userPin = null
        snackbarMessage.value = R.string.message_pin_deleted
    }

    fun setPinValue(pin: String) {
        prefs.userPin = pin
        userPin = pin
        snackbarMessage.value = R.string.message_pin_saved
    }

    fun isLocationSettingsRefused(): Boolean {
        return prefs.locationSettingsRefused
    }

    fun locationSettingsRefused(value: Boolean) {
        prefs.locationSettingsRefused = value
    }

    fun checkUpdateNeeded() {
        val interval = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES)
        if (System.currentTimeMillis() - lastUpdate > interval) {
            loadData()
        }
    }

    fun showPinDialog() {
        pinDialog.value = userPin != null
    }

    fun showDayNightDialog() {
        val mode: AppThemeMode = if (prefs.dayNightMode != null) {
            AppThemeMode.valueOf(prefs.dayNightMode!!)
        } else {
            DayNightUtil.getDefaultMode()
        }
        appThemeDialog.value = mode
    }

    fun showStationDetailDialog(station: Station) {
        stationDetailDialog.value = station
    }

    fun showReportIssueDialog(station: Station) {
        reportIssueDialog.value = station
    }

    fun hideReportIssueDialog() {
        reportIssueDialog.value = null
    }

    fun sendIssueReport(report: IssueReport) {
        sendReport.value = report
    }

    fun navigateToStation(station: Station) {
        navigateTo.value = station.latLng
    }

    fun setDayNightMode(mode: AppThemeMode) {
        if (prefs.dayNightMode != mode.name) {
            prefs.dayNightMode = mode.name
            nightMode.value = mode
            recreateActivity.call()
        }
    }
}
