package eu.istrocode.bajkolator

import android.app.Application
import eu.istrocode.bajkolator.di.component.AppComponent
import eu.istrocode.bajkolator.di.component.DaggerAppComponent
import timber.log.Timber

class LocatorApplication : Application() {

    lateinit var component: AppComponent

    init {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    companion object {
        lateinit var INSTANCE: LocatorApplication
    }

    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        component = DaggerAppComponent.builder()
                .application(this)
                .build()

        component.inject(this)
    }
}
