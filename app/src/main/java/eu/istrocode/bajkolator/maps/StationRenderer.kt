package eu.istrocode.bajkolator.maps

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import eu.istrocode.bajkolator.R
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.util.BitmapUtils.Companion.createBitmap
import eu.istrocode.bajkolator.util.StationUtil.Companion.getMarkerRes


class StationRenderer(private val context: Context) {

    @SuppressLint("InflateParams")
    fun getClusterItemIcon(station: Station): BitmapDescriptor {
        val layout = LayoutInflater.from(context).inflate(R.layout.layout_icon, null)

        if (station.bikes != null) {
            layout.findViewById<TextView>(R.id.text).text = station.bikes.toString()
        } else {
            layout.findViewById<TextView>(R.id.text).height = context.resources.getDimension(R.dimen.unavailable_station_icon_size).toInt()
        }

        val backgroundRes: Int = getMarkerRes(station)
        layout.background = ContextCompat.getDrawable(context, backgroundRes)
        return BitmapDescriptorFactory.fromBitmap(createBitmap(layout))
    }
}