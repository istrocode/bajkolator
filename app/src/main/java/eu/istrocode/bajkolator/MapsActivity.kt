package eu.istrocode.bajkolator

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import eu.istrocode.bajkolator.dto.AppThemeMode
import eu.istrocode.bajkolator.dto.LocationHolder
import eu.istrocode.bajkolator.dto.Station
import eu.istrocode.bajkolator.dto.StationData
import eu.istrocode.bajkolator.dto.report.IssueReport
import eu.istrocode.bajkolator.dto.report.OtherReason
import eu.istrocode.bajkolator.fragment.DayNightDialog
import eu.istrocode.bajkolator.fragment.EnterPinDialog
import eu.istrocode.bajkolator.fragment.ReportIssueDialog
import eu.istrocode.bajkolator.fragment.ShowPinDialog
import eu.istrocode.bajkolator.fragment.StationDetailDialog
import eu.istrocode.bajkolator.maps.StationRenderer
import eu.istrocode.bajkolator.model.LocationViewModel
import eu.istrocode.bajkolator.model.MapViewModel
import eu.istrocode.bajkolator.util.BitmapUtils
import eu.istrocode.bajkolator.util.DateUtils
import eu.istrocode.bajkolator.util.DateUtils.Companion.formatTimeSpannable
import eu.istrocode.bajkolator.util.DayNightUtil
import eu.istrocode.bajkolator.util.LocationUtils
import eu.istrocode.bajkolator.util.PermissionUtils
import eu.istrocode.bajkolator.util.StationUtil
import eu.istrocode.bajkolator.util.distanceTo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.withContext
import timber.log.Timber


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var fab: FloatingActionButton
    private lateinit var refresh: SwipeRefreshLayout

    private lateinit var map: GoogleMap
    private lateinit var model: MapViewModel
    private lateinit var locationViewModel: LocationViewModel
    private var currentLocationMarker: Marker? = null
    private var stationRenderer: StationRenderer? = null
    private var markerList: MutableList<Marker> = ArrayList()
    private var failureSnackbar: Snackbar? = null
    private var playServicesAvailable: Boolean = false

    private val singleThreadContext = newSingleThreadContext("placeMarkers")
    private var placeMarkerScope: Job? = null
    private var selectedStation: Station? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_DayNight)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_map)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        coordinatorLayout = findViewById(R.id.coordinator)
        fab = findViewById(R.id.fab)
        fab.setOnClickListener { showLocation(true, true) }
        refresh = findViewById(R.id.refresh)
        refresh.isEnabled = false
        refresh.setColorSchemeColors(ContextCompat.getColor(this, R.color.secondaryColor))

        model = ViewModelProviders.of(this).get(MapViewModel::class.java)
        model.updateLiveData.observe(this, Observer { refresh.isRefreshing = it })
        model.failureLiveData.observe(this, Observer { showFailureSnack(it) })
        model.snackbarMessage.observe(this, Observer { showMessage(it) })
        model.pinDialog.observe(this, Observer { hasPin ->
            run {
                if (hasPin) {
                    ShowPinDialog.newInstance().show(supportFragmentManager, SHOW_PIN_DIALOG_TAG)
                } else {
                    EnterPinDialog.newInstance().show(supportFragmentManager, ENTER_PIN_DIALOG_TAG)
                }
            }
        })
        model.nightMode.observe(this, Observer {
            val mode = when (it) {
                AppThemeMode.DAY -> {
                    AppCompatDelegate.MODE_NIGHT_NO
                }
                AppThemeMode.NIGHT -> {
                    AppCompatDelegate.MODE_NIGHT_YES
                }
                AppThemeMode.AUTO -> {
                    AppCompatDelegate.MODE_NIGHT_AUTO
                }
                AppThemeMode.SYSTEM -> {
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                }
                else -> AppCompatDelegate.MODE_NIGHT_NO
            }
            AppCompatDelegate.setDefaultNightMode(mode)
        })
        model.appThemeDialog.observe(this, Observer {
            val dialog = DayNightDialog.newInstance(it)
            dialog.show(supportFragmentManager, DAY_NIGHT_DIALOG_TAG)
        })
        model.stationDetailDialog.observe(this, Observer {
            val dialog = StationDetailDialog.newInstance(it)
            dialog.show(supportFragmentManager, STATION_DETAIL_DIALOG_TAG)
        })
        model.reportIssueDialog.observe(this, Observer {
            if (it != null) {
                val dialog = ReportIssueDialog.newInstance(it)
                dialog.show(supportFragmentManager, REPORT_ISSUE_DIALOG_TAG)
            } else {
                val fragment = supportFragmentManager.findFragmentByTag(REPORT_ISSUE_DIALOG_TAG)
                if (fragment != null) {
                    supportFragmentManager
                            .beginTransaction()
                            .remove(fragment)
                            .commit()
                }
            }
        })
        model.sendReport.observe(this, Observer {
            sendEmail(it)
        })
        model.navigateTo.observe(this, Observer {
            navigateTo(it)
        })
        model.recreateActivity.observe(this, Observer {
            recreate()
        })
        playServicesAvailable = isPlayServicesAvailable()
        if (playServicesAvailable) fab.show() else fab.hide()
    }

    private fun sendEmail(report: IssueReport) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.text_report_email_address)))
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.issue_report_email_subject, report.station.title, report.bikeNumber))
        intent.putExtra(Intent.EXTRA_TEXT, buildBody(report))
        try {
            startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.text_send_email)),
                    REQUEST_SEND_EMAIL_REPORT
            )
        } catch (ex: ActivityNotFoundException) {
            Timber.e("No activity to send")
            showMessage(R.string.text_report_email_no_app)
        }
    }

    private fun buildBody(report: IssueReport): String {
        val reason = if (report.reason is OtherReason) {
            report.message
        } else {
            getString(report.reason.getTextResId())
        }
        return getString(R.string.issue_report_email_body,
                report.station.title,
                report.bikeNumber,
                DateUtils.formatDateTime(report.timestamp),
                reason
        )
    }

    private fun navigateTo(latLng: LatLng) {
        val uri = Uri.parse("google.navigation:q=" +
                latLng.latitude + "," + latLng.longitude +
                "&mode=w"
        )
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.setPackage("com.google.android.apps.maps")
        try {
            startActivity(intent)
        } catch (exception: ActivityNotFoundException) {
            Timber.e("Navigation failed: ${exception.message}")
            showMessage(R.string.text_navigation_failed)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (::map.isInitialized) {
            model.position = map.cameraPosition
        }
    }

    override fun onResume() {
        super.onResume()
        if (::map.isInitialized) {
            model.checkUpdateNeeded()
        }
    }

    override fun onStop() {
        super.onStop()
        placeMarkerScope?.cancel()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when {
            item == null -> {
                return super.onOptionsItemSelected(item)
            }
            item.itemId == R.id.menu_open_web -> {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://slovnaftbajk.sk")))
                true
            }
            item.itemId == R.id.update -> {
                model.loadData()
                true
            }
            item.itemId == R.id.menu_pin -> {
                model.showPinDialog()
                true
            }
            item.itemId == R.id.menu_app_day_night -> {
                model.showDayNightDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isRotateGesturesEnabled = false
        map.uiSettings.isMapToolbarEnabled = false
        map.setMinZoomPreference(12.0f)
        map.setMaxZoomPreference(18.0f)
        map.setLatLngBoundsForCameraTarget(LocationUtils.bounds)
        map.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoContents(marker: Marker?): View? {
                return null
            }

            override fun getInfoWindow(marker: Marker): View? {
                return getInfoWindowLayout(marker)
            }
        })
        map.setOnInfoWindowClickListener {
            if (selectedStation != null) {
                model.showStationDetailDialog(selectedStation!!)
            }
            it.hideInfoWindow()
        }
        if (DayNightUtil.isNightModeActive(this)) {
            setDarkMapStyle()
        }
        if (model.position != null) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(model.position))
        } else {
            val cityCenter = LatLng(48.141126, 17.116486)
            val zoom = 14.0f
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(cityCenter, zoom))
        }
        stationRenderer = StationRenderer(this)
        observeMapData()
        if (playServicesAvailable) {
            if (!model.isLocationSettingsRefused()) {
                showLocation(false, false)
            }
        }
    }

    private fun setDarkMapStyle() {
        try {
            val success = map.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style_dark)
            )
            if (!success) {
                Timber.e("Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Timber.e("Can't find style. Error: ${e.message}")
        }
    }

    @SuppressLint("InflateParams")
    private fun getInfoWindowLayout(marker: Marker): View? {
        selectedStation = if (marker == currentLocationMarker) {
            val stationMarker = getClosestStationMarkerInRange(marker, MARKER_MAXIMUM_DISTANCE)
            if (stationMarker != null) {
                stationMarker.tag as Station
            } else {
                return null
            }
        } else {
            marker.tag as Station
        }
        if(selectedStation == null) {
            return null
        }
        val station = selectedStation!!
        val content = LayoutInflater.from(this@MapsActivity).inflate(R.layout.layout_station_info_window, null)
        val textTitle = content.findViewById<TextView>(R.id.text_title)
        val textStationStatus = content.findViewById<TextView>(R.id.text_station_status)
        val textTime = content.findViewById<TextView>(R.id.text_time)
        val imageTime = content.findViewById<ImageView>(R.id.image_time)
        textTitle.text = station.title
        textStationStatus.text = getString(R.string.station_capacity_status, station.bikes
                ?: "?", station.docks)
        textStationStatus.setTextColor(ContextCompat.getColor(this, StationUtil.getColorRes(station)))
        if (station.refresh != null) {
            textTime.visibility = View.VISIBLE
            textTime.setText(formatTimeSpannable(station.refresh), TextView.BufferType.SPANNABLE)
            imageTime.visibility = View.VISIBLE
        } else {
            textTime.visibility = View.GONE
            imageTime.visibility = View.GONE
        }
        return content
    }

    private fun getClosestStationMarkerInRange(marker: Marker, distance: Float): Marker? {
        return markerList.firstOrNull { marker.position.distanceTo(it.position) < distance }
    }

    private fun observeMapData() {
        model.stationsLiveData.observe(this, Observer { it -> placeMarkers(it) })
        model.checkUpdateNeeded()
    }

    private fun placeMarkers(stationData: StationData) {
        placeMarkerScope = GlobalScope.launch(singleThreadContext) {
            withContext(context = Dispatchers.Main) {
                markerList.iterator().forEach { marker ->
                    marker.remove()
                }
                markerList.clear()
            }
            stationData.stations.iterator().forEach { station ->
                val options: MarkerOptions = MarkerOptions()
                        .icon(stationRenderer?.getClusterItemIcon(station))
                        .position(station.latLng)
                        .anchor(0.5f, 0.5f)
                launch(context = Dispatchers.Main) {
                    val marker = map.addMarker(options)
                    marker.alpha = 0f
                    marker.tag = station
                    markerList.add(marker)
                }
            }
            launch(context = Dispatchers.Main) {
                markerList.forEach {
                    it.alpha = 1f
                }
            }
        }
    }

    private fun showLocation(requestPermission: Boolean, forceZoomToLocation: Boolean) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (requestPermission) {
                requestLocationPermission()
            }
        } else {
            observeLocationIfLocationSettingsOk(forceZoomToLocation)
        }
    }

    private fun observeLocationData() {
        if (!::locationViewModel.isInitialized) {
            locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
            locationViewModel.data.observe(this, Observer<LocationHolder> { location ->
                if (location?.location != null) {
                    handleLocationAvailable(location)
                } else {
                    removeMyLocationMarker()
                }
            })
        }
    }

    private fun handleLocationAvailable(location: LocationHolder) {
        val animate = !model.isZoomedToLocation
        updateMyLocationMarker(location, animate)
    }

    private fun updateMyLocationMarker(location: LocationHolder, animate: Boolean) {
        val latLng = LocationUtils.getLatLng(location.location!!)
        if (animate) {
            if (LocationUtils.bounds.contains(latLng)) {
                if (::map.isInitialized) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f), null)
                }
            } else {
                if (!location.cached) {
                    showMessage(R.string.location_too_far)
                }
            }
            if (!location.cached) {
                model.isZoomedToLocation = true
            }
        }
        redrawMyLocation(location)
    }

    private fun redrawMyLocation(location: LocationHolder) {
        val latLng = LocationUtils.getLatLng(location.location!!)
        if (currentLocationMarker != null) {
            currentLocationMarker?.position = latLng
        } else {
            if (::map.isInitialized) {
                currentLocationMarker = map.addMarker(MarkerOptions()
                        .zIndex(1f)
                        .position(latLng)
                        .anchor(0.5.toFloat(), 0.5.toFloat())
                        .icon(BitmapDescriptorFactory.fromBitmap(BitmapUtils.getBitmap(this, R.drawable.ic_my_location_dot))))
            }
        }
    }

    private fun removeMyLocationMarker() {
        if (currentLocationMarker != null) {
            currentLocationMarker?.remove()
            currentLocationMarker = null
        }
    }


    private fun showFailureSnack(visible: Boolean) {
        if (visible) {
            failureSnackbar = Snackbar
                    .make(coordinatorLayout, R.string.data_update_failure, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.data_update_retry) { model.loadData() }
            failureSnackbar?.show()
        } else {
            failureSnackbar?.dismiss()
        }

    }

    private fun showMessage(resId: Int) {
        Snackbar.make(coordinatorLayout, resId, Snackbar.LENGTH_LONG).show()
    }

    private fun requestLocationPermission() {
        PermissionUtils.requestLocationPermission(this, coordinatorLayout, REQUEST_LOCATION_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                observeLocationIfLocationSettingsOk(true)
            } else {
                PermissionUtils.showPermissionNotGranted(coordinatorLayout)
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> {
                if (resultCode == Activity.RESULT_OK) {
                    model.locationSettingsRefused(false)
                    observeLocationData()
                } else {
                    model.locationSettingsRefused(true)
                    showMessage(R.string.message_location_settings_denied)
                }
            }
            REQUEST_SEND_EMAIL_REPORT -> {
                model.hideReportIssueDialog()
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun isPlayServicesAvailable(): Boolean {
        val googleAPI = GoogleApiAvailability.getInstance()
        val result = googleAPI.isGooglePlayServicesAvailable(this)
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        REQUEST_PLAY_SERVICES_RESOLUTION).show()
            }
            return false
        }
        return true
    }

    private fun zoomToLocation() {
        if (::locationViewModel.isInitialized && locationViewModel.data.value?.location != null) {
            updateMyLocationMarker((locationViewModel.data.value!!), true)
        } else {
            showMessage(R.string.location_unavailable)
        }
    }

    private fun observeLocationIfLocationSettingsOk(forceZoomToLocation: Boolean) {
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(LocationRequest().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY))
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
            Timber.i("Location settings $locationSettingsResponse")
            if (forceZoomToLocation) {
                if (::locationViewModel.isInitialized) {
                    zoomToLocation()
                } else {
                    model.isZoomedToLocation = false
                }
            }
            observeLocationData()
        }

        task.addOnFailureListener { exception ->
            Timber.i("Location settings not ok: $exception")
            if (exception is ResolvableApiException) {
                try {
                    Timber.i("Location settings attempt to resolve")
                    exception.startResolutionForResult(this@MapsActivity,
                            REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    Timber.e(sendEx)
                }
            }

        }
    }

    companion object {
        private const val REQUEST_LOCATION_PERMISSION = 1
        private const val REQUEST_PLAY_SERVICES_RESOLUTION = 2
        private const val REQUEST_CHECK_SETTINGS = 3
        private const val REQUEST_SEND_EMAIL_REPORT = 4

        private const val ENTER_PIN_DIALOG_TAG = "ENTER_PIN_DIALOG"
        private const val SHOW_PIN_DIALOG_TAG = "SHOW_PIN_DIALOG"
        private const val STATION_DETAIL_DIALOG_TAG = "STATION_DETAIL_DIALOG"
        private const val DAY_NIGHT_DIALOG_TAG = "DAY_NIGHT_DIALOG"
        private const val REPORT_ISSUE_DIALOG_TAG = "REPORT_ISSUE_DIALOG"

        private const val MARKER_MAXIMUM_DISTANCE = 30f
    }
}
